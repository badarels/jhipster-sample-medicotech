package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.MedicoTechApp;
import com.mycompany.myapp.domain.Personne;
import com.mycompany.myapp.repository.PersonneRepository;
import com.mycompany.myapp.service.PersonneService;
import com.mycompany.myapp.service.dto.PersonneCriteria;
import com.mycompany.myapp.service.PersonneQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.domain.enumeration.sexe;
/**
 * Integration tests for the {@link PersonneResource} REST controller.
 */
@SpringBootTest(classes = MedicoTechApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PersonneResourceIT {

    private static final String DEFAULT_NOM_PERSONNE = "AAAAAAAAAA";
    private static final String UPDATED_NOM_PERSONNE = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSE = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE = "BBBBBBBBBB";

    private static final sexe DEFAULT_SEXE = sexe.MASCULIN;
    private static final sexe UPDATED_SEXE = sexe.FEMININ;

    @Autowired
    private PersonneRepository personneRepository;

    @Autowired
    private PersonneService personneService;

    @Autowired
    private PersonneQueryService personneQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPersonneMockMvc;

    private Personne personne;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Personne createEntity(EntityManager em) {
        Personne personne = new Personne()
            .nomPersonne(DEFAULT_NOM_PERSONNE)
            .prenom(DEFAULT_PRENOM)
            .adresse(DEFAULT_ADRESSE)
            .telephone(DEFAULT_TELEPHONE)
            .sexe(DEFAULT_SEXE);
        return personne;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Personne createUpdatedEntity(EntityManager em) {
        Personne personne = new Personne()
            .nomPersonne(UPDATED_NOM_PERSONNE)
            .prenom(UPDATED_PRENOM)
            .adresse(UPDATED_ADRESSE)
            .telephone(UPDATED_TELEPHONE)
            .sexe(UPDATED_SEXE);
        return personne;
    }

    @BeforeEach
    public void initTest() {
        personne = createEntity(em);
    }

    @Test
    @Transactional
    public void createPersonne() throws Exception {
        int databaseSizeBeforeCreate = personneRepository.findAll().size();
        // Create the Personne
        restPersonneMockMvc.perform(post("/api/personnes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personne)))
            .andExpect(status().isCreated());

        // Validate the Personne in the database
        List<Personne> personneList = personneRepository.findAll();
        assertThat(personneList).hasSize(databaseSizeBeforeCreate + 1);
        Personne testPersonne = personneList.get(personneList.size() - 1);
        assertThat(testPersonne.getNomPersonne()).isEqualTo(DEFAULT_NOM_PERSONNE);
        assertThat(testPersonne.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testPersonne.getAdresse()).isEqualTo(DEFAULT_ADRESSE);
        assertThat(testPersonne.getTelephone()).isEqualTo(DEFAULT_TELEPHONE);
        assertThat(testPersonne.getSexe()).isEqualTo(DEFAULT_SEXE);
    }

    @Test
    @Transactional
    public void createPersonneWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personneRepository.findAll().size();

        // Create the Personne with an existing ID
        personne.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPersonneMockMvc.perform(post("/api/personnes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personne)))
            .andExpect(status().isBadRequest());

        // Validate the Personne in the database
        List<Personne> personneList = personneRepository.findAll();
        assertThat(personneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPersonnes() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList
        restPersonneMockMvc.perform(get("/api/personnes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personne.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomPersonne").value(hasItem(DEFAULT_NOM_PERSONNE)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE)))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
            .andExpect(jsonPath("$.[*].sexe").value(hasItem(DEFAULT_SEXE.toString())));
    }
    
    @Test
    @Transactional
    public void getPersonne() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get the personne
        restPersonneMockMvc.perform(get("/api/personnes/{id}", personne.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(personne.getId().intValue()))
            .andExpect(jsonPath("$.nomPersonne").value(DEFAULT_NOM_PERSONNE))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.adresse").value(DEFAULT_ADRESSE))
            .andExpect(jsonPath("$.telephone").value(DEFAULT_TELEPHONE))
            .andExpect(jsonPath("$.sexe").value(DEFAULT_SEXE.toString()));
    }


    @Test
    @Transactional
    public void getPersonnesByIdFiltering() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        Long id = personne.getId();

        defaultPersonneShouldBeFound("id.equals=" + id);
        defaultPersonneShouldNotBeFound("id.notEquals=" + id);

        defaultPersonneShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPersonneShouldNotBeFound("id.greaterThan=" + id);

        defaultPersonneShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPersonneShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllPersonnesByNomPersonneIsEqualToSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where nomPersonne equals to DEFAULT_NOM_PERSONNE
        defaultPersonneShouldBeFound("nomPersonne.equals=" + DEFAULT_NOM_PERSONNE);

        // Get all the personneList where nomPersonne equals to UPDATED_NOM_PERSONNE
        defaultPersonneShouldNotBeFound("nomPersonne.equals=" + UPDATED_NOM_PERSONNE);
    }

    @Test
    @Transactional
    public void getAllPersonnesByNomPersonneIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where nomPersonne not equals to DEFAULT_NOM_PERSONNE
        defaultPersonneShouldNotBeFound("nomPersonne.notEquals=" + DEFAULT_NOM_PERSONNE);

        // Get all the personneList where nomPersonne not equals to UPDATED_NOM_PERSONNE
        defaultPersonneShouldBeFound("nomPersonne.notEquals=" + UPDATED_NOM_PERSONNE);
    }

    @Test
    @Transactional
    public void getAllPersonnesByNomPersonneIsInShouldWork() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where nomPersonne in DEFAULT_NOM_PERSONNE or UPDATED_NOM_PERSONNE
        defaultPersonneShouldBeFound("nomPersonne.in=" + DEFAULT_NOM_PERSONNE + "," + UPDATED_NOM_PERSONNE);

        // Get all the personneList where nomPersonne equals to UPDATED_NOM_PERSONNE
        defaultPersonneShouldNotBeFound("nomPersonne.in=" + UPDATED_NOM_PERSONNE);
    }

    @Test
    @Transactional
    public void getAllPersonnesByNomPersonneIsNullOrNotNull() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where nomPersonne is not null
        defaultPersonneShouldBeFound("nomPersonne.specified=true");

        // Get all the personneList where nomPersonne is null
        defaultPersonneShouldNotBeFound("nomPersonne.specified=false");
    }
                @Test
    @Transactional
    public void getAllPersonnesByNomPersonneContainsSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where nomPersonne contains DEFAULT_NOM_PERSONNE
        defaultPersonneShouldBeFound("nomPersonne.contains=" + DEFAULT_NOM_PERSONNE);

        // Get all the personneList where nomPersonne contains UPDATED_NOM_PERSONNE
        defaultPersonneShouldNotBeFound("nomPersonne.contains=" + UPDATED_NOM_PERSONNE);
    }

    @Test
    @Transactional
    public void getAllPersonnesByNomPersonneNotContainsSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where nomPersonne does not contain DEFAULT_NOM_PERSONNE
        defaultPersonneShouldNotBeFound("nomPersonne.doesNotContain=" + DEFAULT_NOM_PERSONNE);

        // Get all the personneList where nomPersonne does not contain UPDATED_NOM_PERSONNE
        defaultPersonneShouldBeFound("nomPersonne.doesNotContain=" + UPDATED_NOM_PERSONNE);
    }


    @Test
    @Transactional
    public void getAllPersonnesByPrenomIsEqualToSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where prenom equals to DEFAULT_PRENOM
        defaultPersonneShouldBeFound("prenom.equals=" + DEFAULT_PRENOM);

        // Get all the personneList where prenom equals to UPDATED_PRENOM
        defaultPersonneShouldNotBeFound("prenom.equals=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllPersonnesByPrenomIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where prenom not equals to DEFAULT_PRENOM
        defaultPersonneShouldNotBeFound("prenom.notEquals=" + DEFAULT_PRENOM);

        // Get all the personneList where prenom not equals to UPDATED_PRENOM
        defaultPersonneShouldBeFound("prenom.notEquals=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllPersonnesByPrenomIsInShouldWork() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where prenom in DEFAULT_PRENOM or UPDATED_PRENOM
        defaultPersonneShouldBeFound("prenom.in=" + DEFAULT_PRENOM + "," + UPDATED_PRENOM);

        // Get all the personneList where prenom equals to UPDATED_PRENOM
        defaultPersonneShouldNotBeFound("prenom.in=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllPersonnesByPrenomIsNullOrNotNull() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where prenom is not null
        defaultPersonneShouldBeFound("prenom.specified=true");

        // Get all the personneList where prenom is null
        defaultPersonneShouldNotBeFound("prenom.specified=false");
    }
                @Test
    @Transactional
    public void getAllPersonnesByPrenomContainsSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where prenom contains DEFAULT_PRENOM
        defaultPersonneShouldBeFound("prenom.contains=" + DEFAULT_PRENOM);

        // Get all the personneList where prenom contains UPDATED_PRENOM
        defaultPersonneShouldNotBeFound("prenom.contains=" + UPDATED_PRENOM);
    }

    @Test
    @Transactional
    public void getAllPersonnesByPrenomNotContainsSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where prenom does not contain DEFAULT_PRENOM
        defaultPersonneShouldNotBeFound("prenom.doesNotContain=" + DEFAULT_PRENOM);

        // Get all the personneList where prenom does not contain UPDATED_PRENOM
        defaultPersonneShouldBeFound("prenom.doesNotContain=" + UPDATED_PRENOM);
    }


    @Test
    @Transactional
    public void getAllPersonnesByAdresseIsEqualToSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where adresse equals to DEFAULT_ADRESSE
        defaultPersonneShouldBeFound("adresse.equals=" + DEFAULT_ADRESSE);

        // Get all the personneList where adresse equals to UPDATED_ADRESSE
        defaultPersonneShouldNotBeFound("adresse.equals=" + UPDATED_ADRESSE);
    }

    @Test
    @Transactional
    public void getAllPersonnesByAdresseIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where adresse not equals to DEFAULT_ADRESSE
        defaultPersonneShouldNotBeFound("adresse.notEquals=" + DEFAULT_ADRESSE);

        // Get all the personneList where adresse not equals to UPDATED_ADRESSE
        defaultPersonneShouldBeFound("adresse.notEquals=" + UPDATED_ADRESSE);
    }

    @Test
    @Transactional
    public void getAllPersonnesByAdresseIsInShouldWork() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where adresse in DEFAULT_ADRESSE or UPDATED_ADRESSE
        defaultPersonneShouldBeFound("adresse.in=" + DEFAULT_ADRESSE + "," + UPDATED_ADRESSE);

        // Get all the personneList where adresse equals to UPDATED_ADRESSE
        defaultPersonneShouldNotBeFound("adresse.in=" + UPDATED_ADRESSE);
    }

    @Test
    @Transactional
    public void getAllPersonnesByAdresseIsNullOrNotNull() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where adresse is not null
        defaultPersonneShouldBeFound("adresse.specified=true");

        // Get all the personneList where adresse is null
        defaultPersonneShouldNotBeFound("adresse.specified=false");
    }
                @Test
    @Transactional
    public void getAllPersonnesByAdresseContainsSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where adresse contains DEFAULT_ADRESSE
        defaultPersonneShouldBeFound("adresse.contains=" + DEFAULT_ADRESSE);

        // Get all the personneList where adresse contains UPDATED_ADRESSE
        defaultPersonneShouldNotBeFound("adresse.contains=" + UPDATED_ADRESSE);
    }

    @Test
    @Transactional
    public void getAllPersonnesByAdresseNotContainsSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where adresse does not contain DEFAULT_ADRESSE
        defaultPersonneShouldNotBeFound("adresse.doesNotContain=" + DEFAULT_ADRESSE);

        // Get all the personneList where adresse does not contain UPDATED_ADRESSE
        defaultPersonneShouldBeFound("adresse.doesNotContain=" + UPDATED_ADRESSE);
    }


    @Test
    @Transactional
    public void getAllPersonnesByTelephoneIsEqualToSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where telephone equals to DEFAULT_TELEPHONE
        defaultPersonneShouldBeFound("telephone.equals=" + DEFAULT_TELEPHONE);

        // Get all the personneList where telephone equals to UPDATED_TELEPHONE
        defaultPersonneShouldNotBeFound("telephone.equals=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    public void getAllPersonnesByTelephoneIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where telephone not equals to DEFAULT_TELEPHONE
        defaultPersonneShouldNotBeFound("telephone.notEquals=" + DEFAULT_TELEPHONE);

        // Get all the personneList where telephone not equals to UPDATED_TELEPHONE
        defaultPersonneShouldBeFound("telephone.notEquals=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    public void getAllPersonnesByTelephoneIsInShouldWork() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where telephone in DEFAULT_TELEPHONE or UPDATED_TELEPHONE
        defaultPersonneShouldBeFound("telephone.in=" + DEFAULT_TELEPHONE + "," + UPDATED_TELEPHONE);

        // Get all the personneList where telephone equals to UPDATED_TELEPHONE
        defaultPersonneShouldNotBeFound("telephone.in=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    public void getAllPersonnesByTelephoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where telephone is not null
        defaultPersonneShouldBeFound("telephone.specified=true");

        // Get all the personneList where telephone is null
        defaultPersonneShouldNotBeFound("telephone.specified=false");
    }
                @Test
    @Transactional
    public void getAllPersonnesByTelephoneContainsSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where telephone contains DEFAULT_TELEPHONE
        defaultPersonneShouldBeFound("telephone.contains=" + DEFAULT_TELEPHONE);

        // Get all the personneList where telephone contains UPDATED_TELEPHONE
        defaultPersonneShouldNotBeFound("telephone.contains=" + UPDATED_TELEPHONE);
    }

    @Test
    @Transactional
    public void getAllPersonnesByTelephoneNotContainsSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where telephone does not contain DEFAULT_TELEPHONE
        defaultPersonneShouldNotBeFound("telephone.doesNotContain=" + DEFAULT_TELEPHONE);

        // Get all the personneList where telephone does not contain UPDATED_TELEPHONE
        defaultPersonneShouldBeFound("telephone.doesNotContain=" + UPDATED_TELEPHONE);
    }


    @Test
    @Transactional
    public void getAllPersonnesBySexeIsEqualToSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where sexe equals to DEFAULT_SEXE
        defaultPersonneShouldBeFound("sexe.equals=" + DEFAULT_SEXE);

        // Get all the personneList where sexe equals to UPDATED_SEXE
        defaultPersonneShouldNotBeFound("sexe.equals=" + UPDATED_SEXE);
    }

    @Test
    @Transactional
    public void getAllPersonnesBySexeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where sexe not equals to DEFAULT_SEXE
        defaultPersonneShouldNotBeFound("sexe.notEquals=" + DEFAULT_SEXE);

        // Get all the personneList where sexe not equals to UPDATED_SEXE
        defaultPersonneShouldBeFound("sexe.notEquals=" + UPDATED_SEXE);
    }

    @Test
    @Transactional
    public void getAllPersonnesBySexeIsInShouldWork() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where sexe in DEFAULT_SEXE or UPDATED_SEXE
        defaultPersonneShouldBeFound("sexe.in=" + DEFAULT_SEXE + "," + UPDATED_SEXE);

        // Get all the personneList where sexe equals to UPDATED_SEXE
        defaultPersonneShouldNotBeFound("sexe.in=" + UPDATED_SEXE);
    }

    @Test
    @Transactional
    public void getAllPersonnesBySexeIsNullOrNotNull() throws Exception {
        // Initialize the database
        personneRepository.saveAndFlush(personne);

        // Get all the personneList where sexe is not null
        defaultPersonneShouldBeFound("sexe.specified=true");

        // Get all the personneList where sexe is null
        defaultPersonneShouldNotBeFound("sexe.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPersonneShouldBeFound(String filter) throws Exception {
        restPersonneMockMvc.perform(get("/api/personnes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personne.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomPersonne").value(hasItem(DEFAULT_NOM_PERSONNE)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE)))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
            .andExpect(jsonPath("$.[*].sexe").value(hasItem(DEFAULT_SEXE.toString())));

        // Check, that the count call also returns 1
        restPersonneMockMvc.perform(get("/api/personnes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPersonneShouldNotBeFound(String filter) throws Exception {
        restPersonneMockMvc.perform(get("/api/personnes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPersonneMockMvc.perform(get("/api/personnes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingPersonne() throws Exception {
        // Get the personne
        restPersonneMockMvc.perform(get("/api/personnes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonne() throws Exception {
        // Initialize the database
        personneService.save(personne);

        int databaseSizeBeforeUpdate = personneRepository.findAll().size();

        // Update the personne
        Personne updatedPersonne = personneRepository.findById(personne.getId()).get();
        // Disconnect from session so that the updates on updatedPersonne are not directly saved in db
        em.detach(updatedPersonne);
        updatedPersonne
            .nomPersonne(UPDATED_NOM_PERSONNE)
            .prenom(UPDATED_PRENOM)
            .adresse(UPDATED_ADRESSE)
            .telephone(UPDATED_TELEPHONE)
            .sexe(UPDATED_SEXE);

        restPersonneMockMvc.perform(put("/api/personnes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedPersonne)))
            .andExpect(status().isOk());

        // Validate the Personne in the database
        List<Personne> personneList = personneRepository.findAll();
        assertThat(personneList).hasSize(databaseSizeBeforeUpdate);
        Personne testPersonne = personneList.get(personneList.size() - 1);
        assertThat(testPersonne.getNomPersonne()).isEqualTo(UPDATED_NOM_PERSONNE);
        assertThat(testPersonne.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testPersonne.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testPersonne.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testPersonne.getSexe()).isEqualTo(UPDATED_SEXE);
    }

    @Test
    @Transactional
    public void updateNonExistingPersonne() throws Exception {
        int databaseSizeBeforeUpdate = personneRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPersonneMockMvc.perform(put("/api/personnes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personne)))
            .andExpect(status().isBadRequest());

        // Validate the Personne in the database
        List<Personne> personneList = personneRepository.findAll();
        assertThat(personneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePersonne() throws Exception {
        // Initialize the database
        personneService.save(personne);

        int databaseSizeBeforeDelete = personneRepository.findAll().size();

        // Delete the personne
        restPersonneMockMvc.perform(delete("/api/personnes/{id}", personne.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Personne> personneList = personneRepository.findAll();
        assertThat(personneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
