package com.mycompany.myapp;

import java.util.Scanner;

import com.mycompany.myapp.domain.Patient;
import org.springframework.boot.SpringApplication;

public class Prototype {

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void Menu() {
        int choix, choix2, choix3;
        System.out.println(".............. GESTION CENTRE DE SANTE.........................");
        System.out.println(".............. 1: GESTION CONSULTATION.........................");
        System.out.println(".............. 2: GESTION RENDEZ-VOUS.........................");
        System.out.println(".............. 3: GESTION HOSPITALISATION.........................");
        System.out.println("Faite votre choix");
        Scanner sc = new Scanner(System.in);
        choix = 0;
        choix = sc.nextInt();
        switch (choix) {

            case 1:

                clearScreen();
                System.out.println("...............GESTION CONSULTATION.........................");
                System.out.println("1: ENREGISTRE UNE CONSULTATION");
                System.out.println("2: mODIFIER UNE CONSULTATION");
                System.out.println("3: LISTES DES CONSULTATION");
                choix2 = 0;
                choix2 = sc.nextInt();
                switch (choix2) {
                    case 1:
                        clearScreen();
                        System.out.println("****************ENREGISTRE UNE CONSULTATION******************");
                        break;
                    case 2:
                        clearScreen();
                        System.out.println("****************MODIFIER UNE CONSULTATION******************");
                        break;
                    case 3:
                        clearScreen();
                        System.out.println("****************LISTES DES CONSULTATION******************");
                        break;
                    default:
                        break;
                }
                break;
            case 2:
                clearScreen();
                System.out.println("...............GESTION RENDEZ-VOUS.........................");
                System.out.println("1: ENREGISTRE UN RENDEZ-VOUS");
                System.out.println("2: mODIFIER UN RENDEZ-VOUS");
                System.out.println("3: lISTES DES RENDEZ-VOUS");
                System.out.println("4: ANNULER UN RENDEZ-VOUS");
                choix2 = 0;
                choix2 = sc.nextInt();
                switch (choix2) {
                    case 1:
                        clearScreen();
                        System.out.println("****************ENREGISTRE UN RENDEZ-VOUS******************");
                        break;
                    case 2:
                        clearScreen();
                        System.out.println("****************MODIFIER UN RENDEZ-VOUS******************");
                        break;
                    case 3:
                        clearScreen();
                        System.out.println("****************LISTES DES RENDEZ-VOUS******************");
                        break;
                    case 4:
                        clearScreen();
                        System.out.println("****************ANNULER DES RENDEZ-VOUS******************");
                        break;
                    default:
                        break;
                }
                break;
            case 3:
                clearScreen();
                System.out.println(".............. GESTION HOSPITALISATION.........................");
                System.out.println("1: ENREGISTRE UN HOSPITALISATION");
                System.out.println("2: mODIFIER UN HOSPITALISATION");
                System.out.println("3: lISTES DES HOSPITALISATION");
                choix3 = 0;
                choix3 = sc.nextInt();
                switch (choix3) {
                    case 1:
                        clearScreen();
                        System.out.println("****************ENREGISTRE UNE HOSPITALISATION******************");
                        break;
                    case 2:
                        clearScreen();
                        System.out.println("****************MODIFIER UNE HOSPITALISATION******************");
                        break;
                    case 3:
                        clearScreen();
                        System.out.println("****************LISTES DES HOSPITALISATION******************");
                        break;

                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(MedicoTechApp.class);
        Menu();
    }
}
