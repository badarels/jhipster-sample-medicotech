package com.mycompany.myapp.repository.jdbc;

import java.sql.DriverManager;

public class jdbcBaseMedicotechRepository {
    public static final String DB_USER="root";
    public static final String DB_PASSWORD="";
    public static final String NOM_BASE="medicoTech";
    public static final String IP="localhost";
    public static final String PORT="3306";

    private Connection CreateConnection(){

        try {
            Class C = Class.forName("com.mysql.cj.jdbc.Driver");
            Driver pilote = (Driver) C.newInstance();
            DriverManager.registerDriver(pilote);
            String protocole = "jdbc:mysql:";
            String ip = IP;
            String port = PORT;

            String ChaineConnnection = protocole + "//" + ip + ":" + port + "/" + nomBase;
            String dbUser = DB_USER;
            String dbPassword = DB_PASSWORD;

            //Connexion
            return DriverManager.getConnection(ChaineConnnection, dbUser, dbPassword);
        }catch (Exception ex){
            System.err.println("Une erreur est survenue lors de creation de la connexion");
            System.err.println(ex.getMessage());
            return null;

        }
    }



}
