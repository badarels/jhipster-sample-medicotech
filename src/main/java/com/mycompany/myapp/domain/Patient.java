package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A Patient.
 */
@Entity
@Table(name = "patient")
public class Patient implements Serializable extends Personne {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "age")
    private Integer age;

    @Column(name = "groupe_sanguin")
    private String groupeSanguin;

    @OneToOne(mappedBy = "patient")
    @JsonIgnore
    private Etre etre;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Patient() {
        super();

    }

    public Patient(Long id, String nomPersonne, String prenom, String adresse, String telephone, com.mycompany.myapp.domain.enumeration.sexe sexe ,Long id, Integer age, String groupeSanguin, Etre etre) {
        this.id = id;
        this.age = age;
        this.groupeSanguin = groupeSanguin;
        this.etre = etre;
        super(id,nomPersonne,prenom,adresse,telephone,sexe);

    }

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public Patient age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGroupeSanguin() {
        return groupeSanguin;
    }

    public Patient groupeSanguin(String groupeSanguin) {
        this.groupeSanguin = groupeSanguin;
        return this;
    }

    public void setGroupeSanguin(String groupeSanguin) {
        this.groupeSanguin = groupeSanguin;
    }

    public Etre getEtre() {
        return etre;
    }

    public Patient etre(Etre etre) {
        this.etre = etre;
        return this;
    }

    public void setEtre(Etre etre) {
        this.etre = etre;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Patient)) {
            return false;
        }
        return id != null && id.equals(((Patient) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Patient{" +
            "id=" + getId() +
            ", age=" + getAge() +
            ", groupeSanguin='" + getGroupeSanguin() + "'" +
            "}";
    }
}
