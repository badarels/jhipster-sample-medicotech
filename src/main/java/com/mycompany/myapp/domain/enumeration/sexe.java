package com.mycompany.myapp.domain.enumeration;

/**
 * The sexe enumeration.
 */
public enum sexe {
    MASCULIN, FEMININ
}
