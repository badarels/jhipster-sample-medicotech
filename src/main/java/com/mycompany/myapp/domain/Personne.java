package com.mycompany.myapp.domain;


import javax.persistence.*;

import java.io.Serializable;

import com.mycompany.myapp.domain.enumeration.sexe;

/**
 * A Personne.
 */
@Entity
@Table(name = "personne")
public class abstract Personne implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "nom_personne")
    protected String nomPersonne;

    @Column(name = "prenom")
    protected String prenom;

    @Column(name = "adresse")
    protected String adresse;

    @Column(name = "telephone")
    protected String telephone;

    @Enumerated(EnumType.STRING)
    @Column(name = "sexe")
    protected sexe sexe;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Personne(Long id, String nomPersonne, String prenom, String adresse, String telephone, com.mycompany.myapp.domain.enumeration.sexe sexe) {
        this.id = id;
        this.nomPersonne = nomPersonne;
        this.prenom = prenom;
        this.adresse = adresse;
        this.telephone = telephone;
        this.sexe = sexe;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomPersonne() {
        return nomPersonne;
    }

    public Personne nomPersonne(String nomPersonne) {
        this.nomPersonne = nomPersonne;
        return this;
    }

    public void setNomPersonne(String nomPersonne) {
        this.nomPersonne = nomPersonne;
    }

    public String getPrenom() {
        return prenom;
    }

    public Personne prenom(String prenom) {
        this.prenom = prenom;
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public Personne adresse(String adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelephone() {
        return telephone;
    }

    public Personne telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public sexe getSexe() {
        return sexe;
    }

    public Personne sexe(sexe sexe) {
        this.sexe = sexe;
        return this;
    }

    public void setSexe(sexe sexe) {
        this.sexe = sexe;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Personne)) {
            return false;
        }
        return id != null && id.equals(((Personne) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Personne{" +
            "id=" + getId() +
            ", nomPersonne='" + getNomPersonne() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", sexe='" + getSexe() + "'" +
            "}";
    }
}
