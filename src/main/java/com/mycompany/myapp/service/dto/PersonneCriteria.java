package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.mycompany.myapp.domain.enumeration.sexe;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.mycompany.myapp.domain.Personne} entity. This class is used
 * in {@link com.mycompany.myapp.web.rest.PersonneResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /personnes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PersonneCriteria implements Serializable, Criteria {
    /**
     * Class for filtering sexe
     */
    public static class sexeFilter extends Filter<sexe> {

        public sexeFilter() {
        }

        public sexeFilter(sexeFilter filter) {
            super(filter);
        }

        @Override
        public sexeFilter copy() {
            return new sexeFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nomPersonne;

    private StringFilter prenom;

    private StringFilter adresse;

    private StringFilter telephone;

    private sexeFilter sexe;

    public PersonneCriteria() {
    }

    public PersonneCriteria(PersonneCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.nomPersonne = other.nomPersonne == null ? null : other.nomPersonne.copy();
        this.prenom = other.prenom == null ? null : other.prenom.copy();
        this.adresse = other.adresse == null ? null : other.adresse.copy();
        this.telephone = other.telephone == null ? null : other.telephone.copy();
        this.sexe = other.sexe == null ? null : other.sexe.copy();
    }

    @Override
    public PersonneCriteria copy() {
        return new PersonneCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNomPersonne() {
        return nomPersonne;
    }

    public void setNomPersonne(StringFilter nomPersonne) {
        this.nomPersonne = nomPersonne;
    }

    public StringFilter getPrenom() {
        return prenom;
    }

    public void setPrenom(StringFilter prenom) {
        this.prenom = prenom;
    }

    public StringFilter getAdresse() {
        return adresse;
    }

    public void setAdresse(StringFilter adresse) {
        this.adresse = adresse;
    }

    public StringFilter getTelephone() {
        return telephone;
    }

    public void setTelephone(StringFilter telephone) {
        this.telephone = telephone;
    }

    public sexeFilter getSexe() {
        return sexe;
    }

    public void setSexe(sexeFilter sexe) {
        this.sexe = sexe;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PersonneCriteria that = (PersonneCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nomPersonne, that.nomPersonne) &&
            Objects.equals(prenom, that.prenom) &&
            Objects.equals(adresse, that.adresse) &&
            Objects.equals(telephone, that.telephone) &&
            Objects.equals(sexe, that.sexe);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nomPersonne,
        prenom,
        adresse,
        telephone,
        sexe
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PersonneCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nomPersonne != null ? "nomPersonne=" + nomPersonne + ", " : "") +
                (prenom != null ? "prenom=" + prenom + ", " : "") +
                (adresse != null ? "adresse=" + adresse + ", " : "") +
                (telephone != null ? "telephone=" + telephone + ", " : "") +
                (sexe != null ? "sexe=" + sexe + ", " : "") +
            "}";
    }

}
