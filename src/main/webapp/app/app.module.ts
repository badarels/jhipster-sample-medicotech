import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { MedicoTechSharedModule } from 'app/shared/shared.module';
import { MedicoTechCoreModule } from 'app/core/core.module';
import { MedicoTechAppRoutingModule } from './app-routing.module';
import { MedicoTechHomeModule } from './home/home.module';
import { MedicoTechEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    MedicoTechSharedModule,
    MedicoTechCoreModule,
    MedicoTechHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    MedicoTechEntityModule,
    MedicoTechAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class MedicoTechAppModule {}
