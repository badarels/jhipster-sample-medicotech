import { IEtre } from 'app/shared/model/etre.model';

export interface IPatient {
  id?: number;
  age?: number;
  groupeSanguin?: string;
  etre?: IEtre;
}

export class Patient implements IPatient {
  constructor(public id?: number, public age?: number, public groupeSanguin?: string, public etre?: IEtre) {}
}
