import { sexe } from 'app/shared/model/enumerations/sexe.model';

export interface IPersonne {
  id?: number;
  nomPersonne?: string;
  prenom?: string;
  adresse?: string;
  telephone?: string;
  sexe?: sexe;
}

export class Personne implements IPersonne {
  constructor(
    public id?: number,
    public nomPersonne?: string,
    public prenom?: string,
    public adresse?: string,
    public telephone?: string,
    public sexe?: sexe
  ) {}
}
